namespace Airlines;

public enum EntityType
{
    Airport,
    Airline,
    Flight
}