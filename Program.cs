﻿using Airlines.Services;

namespace Airlines;

class Program
{
    static void Main(string[] args)
    {
        IValidationService validationService = new ValidationService();
        IUserInputService userInputService = new UserInputService(validationService);
        IMenuService menuService = new MenuService();
        
        Application app = new Application(menuService, userInputService);
        app.Run();
    }
}