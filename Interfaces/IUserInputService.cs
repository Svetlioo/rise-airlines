namespace Airlines.Services;

public interface IUserInputService
{
    string ReadUserInput(EntityType entityType);
}   