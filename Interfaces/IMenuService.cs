namespace Airlines.Services;

public interface IMenuService
{
    int ShowMenu(string[] options);
}