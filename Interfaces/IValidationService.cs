namespace Airlines.Services;

public interface IValidationService
{
    (bool,string) IsValid(string item, EntityType entityType);
}