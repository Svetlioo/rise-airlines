namespace Airlines.Services;

public class MenuService : IMenuService
{
    public int ShowMenu(string[] options)
    {
        for (int i = 0; i < options.Length; i++)
        {
            Console.WriteLine($"{i + 1}. {options[i]}");
        }

        Console.Write("Select an option: ");

        int option;
        while (!int.TryParse(Console.ReadLine(), out option) || option < 1 || option > options.Length)
        {
            Console.Write($"Invalid input. Please enter a number between 1 and {options.Length}: ");
        }

        return option;
    }
}