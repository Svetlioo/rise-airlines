namespace Airlines.Services;

public class UserInputService : IUserInputService
{
    private readonly IValidationService _validationService;

    public UserInputService(IValidationService validationService)
    {
        _validationService = validationService;
    }

    public string ReadUserInput(EntityType itemType)
    {
        string item;
        do
        {
            Console.Write($"Enter the name of {itemType}: ");
            item = Console.ReadLine() ?? "";
            if (string.IsNullOrWhiteSpace(item))
            {
                Console.WriteLine("Input cannot be empty. Please try again.");
                continue;
            }
            var (isValid, message) = _validationService.IsValid(item, itemType);
            if (!isValid)
            {
                Console.WriteLine($"{message}");
                Console.WriteLine("Please try again.");
            }
        } while (!_validationService.IsValid(item, itemType).Item1);

        return item;
    }
}