namespace Airlines.Services;

public class ValidationService : IValidationService
{
    public (bool,string) IsValid(string item, EntityType entityType)
    {
        switch (entityType)
        {
            case EntityType.Airport:
                if (item.Length == 3 && item.All(char.IsLetter)) return (true, "Valid airport code."); 
                return (false, "Invalid airport code. It must be 3 letters.");
            
            case EntityType.Airline:
                if (item.Length < 6) return (true, "Valid airline name.");
                return (false, "Invalid airline name. It must be less than 6 characters.");
            
            case EntityType.Flight:
                if (item.All(char.IsLetterOrDigit)) return (true, "Valid flight code.");
                return (false, "Invalid flight code. It must be alphanumeric.");
            
            default:
                return (false, "Invalid entity type.");
        }
    }
}