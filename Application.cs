using Airlines.Services;

namespace Airlines;

public class Application
{
    private readonly IMenuService _menuService;
    private readonly IUserInputService _userInputService;

    private string[] _airports;
    private string[] _airlines;
    private string[] _flights;

    public Application(IMenuService menuService, IUserInputService userInputService)
    {
        _menuService = menuService;
        _userInputService = userInputService;
        _airports = Array.Empty<string>();
        _airlines = Array.Empty<string>();
        _flights = Array.Empty<string>();
    }

    public void Run()
    {
        string[] menuOptions = { "Add an Airport", "Add an Airline", "Add a Flights", "Print All", "Exit" };

        while (true)
        {
            int option = _menuService.ShowMenu(menuOptions);

            switch (option)
            {
                case 1:
                    var airport = _userInputService.ReadUserInput(EntityType.Airport);
                    _airports = AddToArray(_airports, airport);
                    break;
                case 2:
                    var airline = _userInputService.ReadUserInput(EntityType.Airline);
                    _airlines = AddToArray(_airlines, airline);
                    break;
                case 3:
                    var flight = _userInputService.ReadUserInput(EntityType.Flight);
                    _flights = AddToArray(_flights, flight);
                    break;
                case 4:
                    PrintAll();
                    break;
                case 5:
                    Console.WriteLine("Goodbye!");
                    return;
            }
        }
    }
    
    private string[] AddToArray(string[] array, string item)
    {
        string[] newArray = new string[array.Length + 1];
        array.CopyTo(newArray, 0);
        newArray[^1] = item;
        return newArray;
    } // There is a better way to do this, but I will leave it as is for now (no need to implement the list data structure from scratch)

    private void PrintAll()
    {
        Console.WriteLine("Airports: " + string.Join(", ", _airports));
        Console.WriteLine("Airlines: " + string.Join(", ", _airlines));
        Console.WriteLine("Flights: " + string.Join(", ", _flights));
    }
}